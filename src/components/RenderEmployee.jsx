import { Button } from "react-bootstrap";
import {useNavigate} from 'react-router-dom';
const RenderEmployee = (props) => {
  const navigate = useNavigate();
  const { employee, index, setEmployees, employees } = props;
  function deleteEmployee(){
    const newListe = employees.filter((value, item)=> item!== index);
    setEmployees(newListe);
    fetch(`http://localhost:8000/api/employees/${employee._id}`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        }
      }).then((response)=>response.json()).then((message)=>console.log(message));
  }

  return (
    <tr>
      <td>{employee.nom}</td>
      <td>{employee.prenom}</td>
      <td>{employee.age}</td>
      <td>{employee.poste}</td>
      <td>
        <div>
            <h1>{employee.experience.title}</h1>
            <p>{employee.experience.description}</p>
        </div>
      </td>
      <td>
          <Button onClick={()=>navigate(`/formulaire/${employee._id}/${index}`) } variant="success">modifier</Button>
      </td>
      <td>
        <Button onClick={deleteEmployee} variant="danger">supprimer</Button>
      </td>
    </tr>
  );
};

export default RenderEmployee;
