
const FormExperience = (employeeModel, item)=>{
    return (
        <div key={item}>
            <label >{employeeModel.label}</label>
            <input {...employeeModel}  className="form-control" />
        </div >
    );
}

export default FormExperience;