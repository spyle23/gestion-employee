import FormExperience from "./FormExperience";

const Form = (employeeModel, index) => {
  return (
    <div key={index}>
      {employeeModel.name === "experience" ? (
        <>{employeeModel.child.map(FormExperience)}</>
      ) : (
        <>
          <label>{employeeModel.label}</label>
          <input {...employeeModel} className="form-control" />
        </>
      )}
    </div>
  );
};

export default Form;
