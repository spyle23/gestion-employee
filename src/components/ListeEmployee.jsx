import "bootstrap/dist/css/bootstrap.min.css";


import * as ReactBootstrap from "react-bootstrap";
import { useNavigate } from 'react-router-dom';

import RenderEmployee from "./RenderEmployee";
import '../assets/styles/ListeEmployee.css';
const ListeEmployee = (props) => {
  
  const { employees, setEmployees } = props;


  const navigate = useNavigate();
  return (
    <div>
      <ReactBootstrap.Table>
      <thead>
        <tr>
          <th scope="col">nom</th>
          <th scope="col">prenom</th>
          <th scope="col">age</th>
          <th scope="col">poste</th>
          <th scope="col">experience</th>
          <th scope="col"></th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
        {employees.map((employee, index)=> <RenderEmployee employee={employee} key={index} index={index} setEmployees={setEmployees} employees={employees}  />)}
      </tbody>
    </ReactBootstrap.Table>
    <ReactBootstrap.Button   onClick={()=> navigate('/formulaire')} >ajouter un nouveau employé</ReactBootstrap.Button>
    </div>
  );
};

export default ListeEmployee;
