import Form from "../components/Form";
import { EmployeeModels } from "../models/EmployeeModels";
import * as ReactBootstrap from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import '../assets/styles/Formulaire.css';

const Formulaire = (props) => {
  const models = EmployeeModels;
  const navigate = useNavigate();
  const params = useParams();
  const { employees, setEmployees } = props;


  const submitValue = (e) => {
    e.preventDefault();
    const newEmployee = {
        nom: e.target[0].value,
        prenom: e.target[1].value,
        poste: e.target[2].value,
        age: e.target[3].value,
        experience: {
          title: e.target[4].value,
          description: e.target[5].value,
        },
      };
    if (params.id !== undefined) {
        let newListe = employees.map((employee, index)=> parseInt(params.index) === index ? newEmployee : employee);
        setEmployees(e=>newListe);
        console.log(employees);
        fetch(`http://localhost:8000/api/employees/${params.id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(newEmployee)
      }).then((response)=>response.json()).then((message)=>console.log(message));
    } else {
      setEmployees([...employees, newEmployee]);
      fetch('http://localhost:8000/api/employees', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(newEmployee)
      }).then((response)=>response.json()).then((message)=>console.log(message));
    }
    navigate('/');
  };

  return (
    <ReactBootstrap.Form onSubmit={submitValue}>
      {models.map(Form)}
      <ReactBootstrap.Button type="submit">submit</ReactBootstrap.Button>
      <ReactBootstrap.Button onClick={() => navigate("/")}>
        retour
      </ReactBootstrap.Button>
    </ReactBootstrap.Form>
  );
};

export default Formulaire;
