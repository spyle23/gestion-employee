

import 'bootstrap/dist/css/bootstrap.min.css';
import ListeEmployee from '../components/ListeEmployee';
import { Routes, Route } from 'react-router-dom';
import Formulaire from './Formulaire';
import { useEffect, useState } from 'react';

import '../assets/styles/Home.css'


function Home() {
  const [employees, setEmployees] = useState([]);
  useEffect(()=>{
    async function getEmployees(){
      const response = await fetch('http://localhost:8000/api/employees');
      const listes = await response.json();
      setEmployees(listes.employees);
    }
    getEmployees();
  }, []);
  return (
    <div className="App">
      <Routes>      
        <Route path='/' element={<ListeEmployee employees={employees} setEmployees={setEmployees}  />} />
        <Route path='/formulaire' element={<Formulaire employees={employees} setEmployees={setEmployees} />} />
        <Route path='/formulaire/:id/:index' element={<Formulaire employees={employees} setEmployees={setEmployees}  />} />
      </Routes>
      
    </div>
  );
}

export default Home;
