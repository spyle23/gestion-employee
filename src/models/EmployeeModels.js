
export const EmployeeModels = [
    {
        name:"nom",
        type: "text",
        placeholder:"entrer votre nom",
        label: "nom"
    },
    {
        name:"prenom",
        type:"text",
        placeholder: "votre prénom",
        label: "prenom"
    },
    {
        name: "poste",
        type: "text",
        placeholder:"votre poste",
        label: "poste"
    },
    {
        name: "age",
        type: "number",
        min: 15,
        label: "age"
    },
    {
        name:"experience",
        child:[
            {
                name:"experience-title",
                type: "text",
                label: "titre",
                placeholder: "titre de l'expérience"
            },
            {
                name: "experience-description",
                type: "text",
                label: "description",
                placeholder: "description de l'expérience"
            }
        ]
    }
];